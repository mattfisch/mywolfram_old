package codes.mattfisch.mywolframfullresults;

import codes.mattfisch.mywolframshared.model.WaQueryResult;
import codes.mattfisch.mywolframshared.networking.WaAPICall;

/**
 * This class enables its user to query Wolfram Alphas Full Results Answer API
 */
public class FullResultApi extends WaAPICall<WaQueryResult> {

    public FullResultApi(String _apiKey, String _endpoint) {
        super(_apiKey, _endpoint);
    }

    @Override
    public Class<WaQueryResult> getResponseType() {
        return WaQueryResult.class;
    }
}

package codes.mattfisch.mywolframfullresults;

import org.junit.Test;

import codes.mattfisch.mywolframshared.model.WAQueryResult;
import codes.mattfisch.mywolframshared.networking.APICall.OnApiResponse;

public class FullResultsUnitTests {

    @Test
    public void testFullResultsApi() throws Exception {
        FullResultApi api = new FullResultApi("WRU2J4-57WA6EHGHH", "query");

        api.setOnApiResponse(new OnApiResponse<WAQueryResult>() {
            @Override
            public void response(WAQueryResult _waQueryResult) {
                System.out.println(_waQueryResult);
            }
        });

        api.ask("What is the mass of Saturn?");

        Thread.sleep(5000);
    }
}
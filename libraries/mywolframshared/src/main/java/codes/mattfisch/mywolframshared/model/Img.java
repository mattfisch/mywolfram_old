package codes.mattfisch.mywolframshared.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Img {
    @SerializedName("src")
    @Expose
    private String mSrc;
    @SerializedName("alt")
    @Expose
    private String mAlt;
    @SerializedName("title")
    @Expose
    private String mTitle;
    @SerializedName("width")
    @Expose
    private int mWidth;
    @SerializedName("height")
    @Expose
    private int mHeight;

    public String getSrc() {
        return mSrc;
    }

    public void setSrc(String _src) {
        mSrc = _src;
    }

    public String getAlt() {
        return mAlt;
    }

    public void setAlt(String _alt) {
        mAlt = _alt;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String _title) {
        mTitle = _title;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int _width) {
        mWidth = _width;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int _height) {
        mHeight = _height;
    }

    @Override
    public String toString() {
        return "Img{" +
                "src='" + mSrc + '\'' +
                ", alt='" + mAlt + '\'' +
                ", title='" + mTitle + '\'' +
                ", width=" + mWidth +
                ", height=" + mHeight +
                '}';
    }
}

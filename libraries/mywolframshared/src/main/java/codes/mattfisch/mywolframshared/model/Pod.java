package codes.mattfisch.mywolframshared.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Pod {
    @SerializedName("title")
    @Expose
    private String mTitle;
    @SerializedName("scanner")
    @Expose
    private String mScanner;
    @SerializedName("id")
    @Expose
    private String mId;
    @SerializedName("position")
    @Expose
    private int mPosition;
    @SerializedName("error")
    @Expose
    private boolean mError;
    @SerializedName("numsubpods")
    @Expose
    private int mNumsubpods;
    @SerializedName("subpods")
    @Expose
    private List<Subpod> mSubpods;
    @SerializedName("primary")
    @Expose
    private boolean mPrimary;
    @SerializedName("states")
    @Expose
    private List<State> mStates;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String _title) {
        mTitle = _title;
    }

    public String getScanner() {
        return mScanner;
    }

    public void setScanner(String _scanner) {
        mScanner = _scanner;
    }

    public String getId() {
        return mId;
    }

    public void setId(String _id) {
        mId = _id;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int _position) {
        mPosition = _position;
    }

    public boolean isError() {
        return mError;
    }

    public void setError(boolean _error) {
        mError = _error;
    }

    public int getNumsubpods() {
        return mNumsubpods;
    }

    public void setNumsubpods(int _numsubpods) {
        mNumsubpods = _numsubpods;
    }

    public List<Subpod> getSubpods() {
        return mSubpods;
    }

    public void setSubpods(List<Subpod> _subpodList) {
        mSubpods = _subpodList;
    }

    public boolean isPrimary() {
        return mPrimary;
    }

    public void setPrimary(boolean _primary) {
        mPrimary = _primary;
    }

    public List<State> getStates() {
        return mStates;
    }

    public void setStates(List<State> _stateList) {
        mStates = _stateList;
    }

    @Override
    public String toString() {
        return "Pod{" +
                "title='" + mTitle + '\'' +
                ", scanner='" + mScanner + '\'' +
                ", id='" + mId + '\'' +
                ", position=" + mPosition +
                ", error=" + mError +
                ", numsubpods=" + mNumsubpods +
                ", subpods=" + mSubpods +
                ", primary=" + mPrimary +
                ", states=" + mStates +
                '}';
    }
}

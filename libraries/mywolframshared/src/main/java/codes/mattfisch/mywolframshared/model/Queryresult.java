package codes.mattfisch.mywolframshared.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Queryresult {
    @SerializedName("success")
    @Expose
    private boolean mSuccess;

    @SerializedName("error")
    @Expose
    private boolean mError;

    @SerializedName("numpods")
    @Expose
    private int mNumpods;

    @SerializedName("datatypes")
    @Expose
    private String mDatatypes;

    @SerializedName("timedout")
    @Expose
    private String mTimedout;

    @SerializedName("timedoutpods")
    @Expose
    private String mTimedoutpods;

    @SerializedName("timing")
    @Expose
    private double mTiming;

    @SerializedName("parsetiming")
    @Expose
    private double mParsetiming;

    @SerializedName("parsetimedout")
    @Expose
    private boolean mParsetimedout;

    @SerializedName("recalculate")
    @Expose
    private String mRecalculate;

    @SerializedName("id")
    @Expose
    private String mId;

    @SerializedName("host")
    @Expose
    private String mHost;

    @SerializedName("server")
    @Expose
    private String mServer;

    @SerializedName("related")
    @Expose
    private String mRelated;

    @SerializedName("version")
    @Expose
    private String mVersion;

    @SerializedName("pods")
    @Expose
    private List<Pod> mPods;

    public boolean isSuccess() {
        return mSuccess;
    }

    public void setSuccess(boolean _success) {
        mSuccess = _success;
    }

    public boolean isError() {
        return mError;
    }

    public void setError(boolean _error) {
        mError = _error;
    }

    public int getNumpods() {
        return mNumpods;
    }

    public void setNumpods(int _numpods) {
        mNumpods = _numpods;
    }

    public String getDatatypes() {
        return mDatatypes;
    }

    public void setDatatypes(String _datatypes) {
        mDatatypes = _datatypes;
    }

    public String getTimedout() {
        return mTimedout;
    }

    public void setTimedout(String _timedout) {
        mTimedout = _timedout;
    }

    public String getTimedoutpods() {
        return mTimedoutpods;
    }

    public void setTimedoutpods(String _timedoutpods) {
        mTimedoutpods = _timedoutpods;
    }

    public double getTiming() {
        return mTiming;
    }

    public void setTiming(double _timing) {
        mTiming = _timing;
    }

    public double getParsetiming() {
        return mParsetiming;
    }

    public void setParsetiming(double _parsetiming) {
        mParsetiming = _parsetiming;
    }

    public boolean isParsetimedout() {
        return mParsetimedout;
    }

    public void setParsetimedout(boolean _parsetimedout) {
        mParsetimedout = _parsetimedout;
    }

    public String getRecalculate() {
        return mRecalculate;
    }

    public void setRecalculate(String _recalculate) {
        mRecalculate = _recalculate;
    }

    public String getId() {
        return mId;
    }

    public void setId(String _id) {
        this.mId = _id;
    }

    public String getHost() {
        return mHost;
    }

    public void setHost(String _host) {
        mHost = _host;
    }

    public String getServer() {
        return mServer;
    }

    public void setServer(String _server) {
        mServer = _server;
    }

    public String getRelated() {
        return mRelated;
    }

    public void setRelated(String _related) {
        mRelated = _related;
    }

    public String getVersion() {
        return mVersion;
    }

    public void setVersion(String _version) {
        mVersion = _version;
    }

    public List<Pod> getPods() {
        return mPods;
    }

    public void setPods(List<Pod> _podList) {
        mPods = _podList;
    }

    @Override
    public String toString() {
        return "Queryresult{" +
                "mSuccess=" + mSuccess +
                ", mError=" + mError +
                ", mNumpods=" + mNumpods +
                ", mDatatypes='" + mDatatypes + '\'' +
                ", mTimedout='" + mTimedout + '\'' +
                ", mTimedoutpods='" + mTimedoutpods + '\'' +
                ", mTiming=" + mTiming +
                ", mParsetiming=" + mParsetiming +
                ", mParsetimedout=" + mParsetimedout +
                ", mRecalculate='" + mRecalculate + '\'' +
                ", mId='" + mId + '\'' +
                ", mHost='" + mHost + '\'' +
                ", mServer='" + mServer + '\'' +
                ", mRelated='" + mRelated + '\'' +
                ", mVersion='" + mVersion + '\'' +
                ", mPods=" + mPods +
                '}';
    }
}

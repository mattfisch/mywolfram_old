package codes.mattfisch.mywolframshared.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class State {
    @SerializedName("name")
    @Expose
    private String mName;
    @SerializedName("input")
    @Expose
    private String mInput;

    public String getName() {
        return mName;
    }

    public void setName(String _name) {
        mName = _name;
    }

    public String getInput() {
        return mInput;
    }

    public void setInput(String _input) {
        mInput = _input;
    }

    @Override
    public String toString() {
        return "State{" +
                "name='" + mName + '\'' +
                ", input='" + mInput + '\'' +
                '}';
    }
}

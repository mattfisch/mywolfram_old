package codes.mattfisch.mywolframshared.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subpod {
    @SerializedName("title")
    @Expose
    private String mTitle;
    @SerializedName("img")
    @Expose
    private Img mImg;
    @SerializedName("plaintext")
    @Expose
    private String mPlaintext;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String _title) {
        mTitle = _title;
    }

    public Img getImg() {
        return mImg;
    }

    public void setImg(Img img) {
        mImg = img;
    }

    public String getPlaintext() {
        return mPlaintext;
    }

    public void setPlaintext(String _plaintext) {
        mPlaintext = _plaintext;
    }

    @Override
    public String toString() {
        return "Subpod{" +
                "title='" + mTitle + '\'' +
                ", img=" + mImg +
                ", plaintext='" + mPlaintext + '\'' +
                '}';
    }
}

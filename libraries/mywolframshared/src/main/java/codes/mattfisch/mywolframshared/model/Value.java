package codes.mattfisch.mywolframshared.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {
    @SerializedName("name")
    @Expose
    private String mName;
    @SerializedName("desc")
    @Expose
    private String mDesc;
    @SerializedName("input")
    @Expose
    private String mInput;

    public String getName() {
        return mName;
    }

    public void setName(String _name) {
        mName = _name;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setDesc(String _desc) {
        mDesc = _desc;
    }

    public String getInput() {
        return mInput;
    }

    public void setInput(String _input) {
        mInput = _input;
    }

    @Override
    public String toString() {
        return "Value{" +
                "name='" + mName + '\'' +
                ", desc='" + mDesc + '\'' +
                ", input='" + mInput + '\'' +
                '}';
    }
}

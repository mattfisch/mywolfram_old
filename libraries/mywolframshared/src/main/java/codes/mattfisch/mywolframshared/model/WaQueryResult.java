package codes.mattfisch.mywolframshared.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WaQueryResult {
    @SerializedName("queryresult")
    @Expose
    private Queryresult mQueryresult;

    public Queryresult getQueryresult() {
        return mQueryresult;
    }

    public void setQueryresult(Queryresult _queryresult) {
        mQueryresult = _queryresult;
    }

    @Override
    public String toString() {
        return "FullResult{" +
                "queryresult=" + mQueryresult +
                '}';
    }
}

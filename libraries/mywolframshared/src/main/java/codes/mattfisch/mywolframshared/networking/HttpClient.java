package codes.mattfisch.mywolframshared.networking;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import okhttp3.logging.HttpLoggingInterceptor.Logger;

public class HttpClient {

    /**
     * @return HTTPLoggingInterceptor, which intercepts all outgoing and incoming HTTP-traffic
     */
    protected HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(
                new Logger() {
                    @Override
                    public void log(String _message) {
                        System.out.println(_message);
                    }
                });
        interceptor.setLevel(Level.BASIC);
        return interceptor;
    }

    /**
     * @param _appId WolframAlphas API key
     * @return a OkHttpClient, for sending and receiving HTTP-requests/responses
     */
    protected OkHttpClient okHttpClient(final String _appId) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        HttpUrl originalHttpUrl = original.url();

                        //add application id, which is needed in EVERY wolfram alpha request
                        //to the url
                        HttpUrl url = originalHttpUrl.newBuilder()
                                .addQueryParameter("appid", _appId)
                                .build();

                        System.out.println(url);

                        // Request customization: add request headers
                        Builder requestBuilder = original.newBuilder()
                                .url(url);

                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                })
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .build();
    }
}


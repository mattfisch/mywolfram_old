package codes.mattfisch.mywolframshared.networking;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface IWaService {

    /**
     * @param _question   The question, which to ask the api
     * @param _parameters optional parameters
     * @return An @{@link Call with @{@link ResponseBody} as value
     */
    @GET("dummy")
    Call<ResponseBody> ask(
            @Query("input") String _question,
            @QueryMap Map<String, String> _parameters);

}

package codes.mattfisch.mywolframshared.networking;

import com.google.gson.Gson;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;

public abstract class WaAPICall<T> {

    public interface OnApiResponse<T> {
        void response(T _response);
    }

    private IWaService mWolframAlphaService;

    private OnApiResponse mOnApiResponse;

    public WaAPICall(String _apiKey, String _endpoint) {
        WaHttpService service = new WaHttpService(_apiKey);
        mWolframAlphaService = service.createWolframAlphaApiService(IWaService.class);

        try {
            Method method = IWaService.class.getMethod("ask", String.class, Map.class);
            final GET methodAnnotation = method.getAnnotation(GET.class);
            changeAnnotationValue(methodAnnotation, "value", _endpoint + "?output=json");
        } catch (NoSuchMethodException _e) {
            _e.printStackTrace();
        }
    }

    protected abstract Class<T> getResponseType();

    public void ask(String _question) {
        ask(_question, new HashMap<String, String>());
    }

    public void ask(String _question, Map<String, String> _parameters) {
        Call<ResponseBody> request = mWolframAlphaService.ask(_question, _parameters);
        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> _call, Response<ResponseBody> _response) {
                try {
                    //String and ResponseBody implementations return the raw results
                    if (getResponseType() == String.class) {
                        if (mOnApiResponse != null)
                            mOnApiResponse.response((T) _response.body().string());
                    } else if (getResponseType() == ResponseBody.class) {
                        if (mOnApiResponse != null)
                            mOnApiResponse.response((T) _response.body());
                    } else {
                        Gson gson = new Gson();
                        T result = gson.fromJson(_response.body().string(), getResponseType());
                        if (mOnApiResponse != null) {
                            mOnApiResponse.response(result);
                        }
                    }
                } catch (IOException _e) {
                    _e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> _call, Throwable _t) {
                _t.printStackTrace();
            }
        });
    }

    /**
     * Set the callback, which will receive the results of the API requests
     *
     * @param _onApiResponse
     */
    public void setOnApiResponse(OnApiResponse<T> _onApiResponse) {
        mOnApiResponse = _onApiResponse;
    }

    /**
     * Changes the {@link Annotation} value for the given key of the given {@link Annotation}
     */
    private void changeAnnotationValue(Annotation _annotation, String _key, String _newValue) {
        Object handler = Proxy.getInvocationHandler(_annotation);
        Field f;
        try {
            f = handler.getClass().getDeclaredField("memberValues");
        } catch (NoSuchFieldException | SecurityException e) {
            throw new IllegalStateException(e);
        }
        f.setAccessible(true);
        Map<String, Object> memberValues;
        try {
            memberValues = (Map<String, Object>) f.get(handler);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        Object oldValue = memberValues.get(_key);
        if (oldValue == null || oldValue.getClass() != _newValue.getClass()) {
            throw new IllegalArgumentException();
        }
        memberValues.put(_key, _newValue);
    }
}

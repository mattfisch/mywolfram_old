package codes.mattfisch.mywolframshared.networking;


import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Creates an HTTP-GET requests performing service using {@link Retrofit}, which targets the Wolfram
 * Alphas web API endpoint currently at "http://api.wolframalpha.com/v2/"
 */
public class WaHttpService {

    private final String mWaKey;
    private String mWaEndpoint;
    private Retrofit mRetrofit;

    /**
     * Construct the service with the provided Wolfram Alpha Api key
     *
     * @param _waKey
     */
    public WaHttpService(String _waKey) {
        mWaEndpoint = "http://api.wolframalpha.com/v2/";
        mWaKey = _waKey;
        mRetrofit = createRetrofitInstance();
    }

    /**
     * This library might get out of date, when the Wolfram Alpha endpoint changes in the future
     * to make it usable until an updated version is availabe, this method is provided to override
     * the current api's endpoint
     *
     * @param _endpoint the Wolfram Alpha Api's current endpoint
     */
    public void setCustomEndpoint(String _endpoint) {
        mWaEndpoint = _endpoint;
        mRetrofit = this.createRetrofitInstance();
    }

    /**
     * @return a new {@link Retrofit} instance
     */
    private Retrofit createRetrofitInstance() {
        return new Builder()
                .baseUrl(mWaEndpoint)
                .addConverterFactory(gsonConverterFactory())
                .client(new HttpClient().okHttpClient(mWaKey))
                .build();
    }

    /**
     * @return a {@link com.google.gson.Gson} converter factory for
     * converting JSON-responses into GSON-Annotated POJO's
     */
    private static GsonConverterFactory gsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    /**
     * Returns an instance of the provided class as HttpService to query the Wolfram Alpha Api
     *
     * @param _serviceClass the interface, from which the service should be extracted
     * @return an instance of the service
     */
    public <T> T createWolframAlphaApiService(Class<T> _serviceClass) {
        return mRetrofit.create(_serviceClass);
    }
}
package codes.mattfisch.mywolframshortanswers;

import codes.mattfisch.mywolframshared.networking.WaAPICall;

/**
 * This class enables its user to query Wolfram Alphas Short Answer API
 */
public class ShortAnswerApi extends WaAPICall<String> {

    public ShortAnswerApi(String _apiKey) {
        super(_apiKey, "result");
    }

    @Override
    public Class<String> getResponseType() {
        return String.class;
    }
}

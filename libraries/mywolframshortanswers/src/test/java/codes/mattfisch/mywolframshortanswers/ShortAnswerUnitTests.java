package codes.mattfisch.mywolframshortanswers;

import org.junit.Test;

import codes.mattfisch.mywolframshared.networking.APICall.OnApiResponse;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ShortAnswerUnitTests {

    @Test
    public void testShortAnswerApi() throws InterruptedException {
        ShortAnswerApi api = new ShortAnswerApi("WRU2J4-57WA6EHGHH");

        api.setOnApiResponse(new OnApiResponse<String>() {
            @Override
            public void response(String _s) {
                System.out.println(_s);
            }
        });

        api.ask("How big is the eiffel tower?");


        //give the networking thread two seconds to retrieve the information
        Thread.sleep(2000);


    }
}
package codes.mattfisch.mywolframsimpleimages;

import codes.mattfisch.mywolframshared.networking.WaAPICall;
import okhttp3.ResponseBody;

/**
 * This class enables its user to query Wolfram Alphas Simple API, which returns the information
 * compiled into an Image
 */
public class SimpleImageApi extends WaAPICall<ResponseBody> {

    public SimpleImageApi(String _apiKey, String _endpoint) {
        super(_apiKey, _endpoint);
    }

    @Override
    public Class<ResponseBody> getResponseType() {
        return ResponseBody.class;
    }

    @Override
    public void setOnApiResponse(OnApiResponse<ResponseBody> _onApiResponse) {
        super.setOnApiResponse(_onApiResponse);
    }
}

package codes.mattfisch.mywolframsimpleimages;

import org.junit.Test;

import java.io.IOException;

import codes.mattfisch.mywolframshared.networking.WaAPICall.OnApiResponse;
import okhttp3.ResponseBody;

public class SimpleImageUnitTests {

    @Test
    public void testSimpleImageAPI() throws Exception {
        SimpleImageApi api = new SimpleImageApi("WRU2J4-57WA6EHGHH", "simple");

        api.setOnApiResponse(new OnApiResponse<ResponseBody>() {
            @Override
            public void response(ResponseBody _responseBody) {
                try {
                    System.out.println(_responseBody.string());
                } catch (IOException _e) {
                    _e.printStackTrace();
                }
            }
        });

        api.ask("How far is it from Germany to China?");

        Thread.sleep(20000);
    }
}
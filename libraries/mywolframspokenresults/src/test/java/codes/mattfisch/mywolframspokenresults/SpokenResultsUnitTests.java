package codes.mattfisch.mywolframspokenresults;

import org.junit.Test;

import codes.mattfisch.mywolframshared.networking.APICall.OnApiResponse;

public class SpokenResultsUnitTests {
    @Test
    public void testSpokenAnswersApi() throws Exception {
        SpokenResultApi api = new SpokenResultApi("WRU2J4-57WA6EHGHH", "spoken");

        api.setOnApiResponse(new OnApiResponse<String>() {
            @Override
            public void response(String _s) {
                System.out.println(_s);
            }
        });

        api.ask("How big is the eiffel tower?");

        //give the networking thread two seconds to retrieve the information
        Thread.sleep(2000);
    }
}